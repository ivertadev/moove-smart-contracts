// contracts/MVTToken.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * MVT Token
 * @author Moove
 */
contract MVTToken is ERC20, Ownable {
    constructor() ERC20("MVT", "MVT") {
        _mint(msg.sender, 1000000000 * 10**decimals());
    }
}
