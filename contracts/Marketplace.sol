// contracts/Marketplace.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

/**
 * Marketplace contract for Moove project
 * @author Moove
 */
contract Marketplace is ReentrancyGuard, IERC721Receiver, ERC721Holder {
    using Counters for Counters.Counter;
    using SafeERC20 for IERC20;

    /// @notice Platform stats variables
    Counters.Counter private _nftsSold;
    Counters.Counter private _nftCount;

    /// @notice Platform fee
    uint256 public LISTING_FEE = 0.0001 ether;

    /// @notice Platform fee receipient
    address payable private _marketOwner;

    /// @notice NFT address -> Token ID -> Listing item
    mapping(address => mapping(uint256 => NFT)) private _listOfNfts;

    /// @notice Price address -> isAvailable
    mapping(address => bool) private _allowedPriceAssets;

    /// @notice NFT address -> isAvailable
    mapping(address => bool) private _allowedNftContracts;

    /// @notice Structure for listed items
    struct NFT {
        address nftContract;
        uint256 tokenId;
        address payable seller;
        address payable owner;
        address payToken;
        uint256 price;
        uint256 listingFee;
        bool listed;
    }

    /// @notice Events for the contract
    event NFTListed(
        address nftContract,
        uint256 tokenId,
        address seller,
        address owner,
        uint256 price
    );
    event NFTSold(
        address nftContract,
        uint256 tokenId,
        address seller,
        address owner,
        uint256 price
    );
    event NFTUpdate(
        address nftContract,
        uint256 tokenId,
        address seller,
        uint256 price
    );
    event NFTCancel(address nftContract, uint256 tokenId, address seller);

    /// @notice Modifiers for the contract
    modifier isValidListing(address _nftAddress, address _payToken) {
        require(
            _allowedNftContracts[_nftAddress] == true,
            "NFT smart contract is not allowed"
        );
        require(
            _allowedPriceAssets[_payToken] == true,
            "Price asset is not allowed"
        );
        _;
    }
    modifier isSellerListed(address _nftContract, uint256 _tokenId) {
        NFT storage nft = _listOfNfts[_nftContract][_tokenId];
        require(nft.listed, "NFT is not listed");
        require(nft.seller == msg.sender, "Only seller can use this function");
        _;
    }
    modifier isPriceValid(uint256 _price) {
        require(_price > 0, "Price must be at least 1 wei");
        _;
    }
    modifier isValidPurchase(
        address _nftContract,
        uint256 _tokenId,
        address _buyer
    ) {
        NFT storage nft = _listOfNfts[_nftContract][_tokenId];
        require(_buyer != nft.seller, "You cannot buy your selling NFT");
        require(nft.listed == true, "The NFT is not listed on the marketplace");
        _;
    }
    modifier onlyOwner() {
        require(msg.sender == _marketOwner, "Only owner can use this function");
        _;
    }

    /// @notice Contract initializer
    constructor() {
        _marketOwner = payable(msg.sender);
    }

    /**
    @notice Method for listing NFT
    @param _nftContract Address of NFT contract
    @param _tokenId Token ID of NFT
    @param _price Price for NFT
    @param _payToken Paying token
    */
    function listNft(
        address _nftContract,
        uint256 _tokenId,
        uint256 _price,
        address _payToken
    )
        public
        nonReentrant
        isValidListing(_nftContract, _payToken)
        isPriceValid(_price)
    {
        // Transfer listing fee to smart contract
        IERC20(_payToken).safeTransferFrom(
            msg.sender,
            address(this),
            LISTING_FEE
        );

        // Transfer NFT to smart contract
        IERC721(_nftContract).safeTransferFrom(
            msg.sender,
            address(this),
            _tokenId
        );

        _nftCount.increment();

        // Add listing to marketplace mapping
        _listOfNfts[_nftContract][_tokenId] = NFT(
            _nftContract,
            _tokenId,
            payable(msg.sender),
            payable(address(this)),
            _payToken,
            _price,
            LISTING_FEE,
            true
        );

        emit NFTListed(
            _nftContract,
            _tokenId,
            msg.sender,
            address(this),
            _price
        );
    }

    /**
    @notice Method for updating listed NFT
    @param _nftContract Address of NFT contract
    @param _tokenId Token ID of NFT
    @param _newPrice New sale price
    */
    function updatePrice(
        address _nftContract,
        uint256 _tokenId,
        uint256 _newPrice
    )
        public
        nonReentrant
        isSellerListed(_nftContract, _tokenId)
        isPriceValid(_newPrice)
    {
        NFT storage nft = _listOfNfts[_nftContract][_tokenId];

        nft.price = _newPrice;

        emit NFTUpdate(_nftContract, nft.tokenId, nft.seller, nft.price);
    }

    /**
    @notice Method for canceling listed NFT
    @param _nftContract Address of NFT contract
    @param _tokenId Token ID of NFT
    */
    function cancelListing(address _nftContract, uint256 _tokenId)
        public
        nonReentrant
        isSellerListed(_nftContract, _tokenId)
    {
        NFT storage nft = _listOfNfts[_nftContract][_tokenId];

        // Transfer NFT back to seller
        IERC721(_nftContract).transferFrom(
            address(this),
            nft.seller,
            nft.tokenId
        );

        // Transfer listing fee back to seller
        IERC20(nft.payToken).safeTransfer(nft.seller, nft.listingFee);

        nft.listed = false;
        nft.owner = nft.seller;

        emit NFTCancel(_nftContract, nft.tokenId, nft.seller);
    }

    /**
    @notice Method for buying listed NFT
    @param _nftContract NFT contract address
    @param _tokenId TokenId
    */
    function buyNft(address _nftContract, uint256 _tokenId)
        public
        payable
        nonReentrant
        isValidPurchase(_nftContract, _tokenId, msg.sender)
    {
        NFT storage nft = _listOfNfts[_nftContract][_tokenId];
        address payable buyer = payable(msg.sender);

        // Transfer price to seller
        IERC20(nft.payToken).safeTransferFrom(buyer, nft.seller, nft.price);

        // Transfer listing fee to marketplace owner
        IERC20(nft.payToken).safeTransfer(_marketOwner, nft.listingFee);

        // Transfer NFT to buyer
        IERC721(_nftContract).safeTransferFrom(
            address(this),
            buyer,
            nft.tokenId
        );

        nft.owner = buyer;
        nft.listed = false;

        _nftsSold.increment();

        emit NFTSold(_nftContract, nft.tokenId, nft.seller, buyer, nft.price);
    }

    /**
     @notice Method for updating platform fee
     @dev OnlyOwner
     @param _platformFee uint256 the platform fee to set
     */
    function updateListingFee(uint256 _platformFee) public onlyOwner {
        LISTING_FEE = _platformFee;
    }

    /**
     @notice Method for updating supported price assets
     @dev OnlyOwner
     @param _priceAsset Address of price asset contract
     */
    function addSupportedPriceAsset(address _priceAsset) public onlyOwner {
        _allowedPriceAssets[_priceAsset] = true;
    }

    /**
     @notice Method for updating supported NFT contracts
     @dev OnlyOwner
     @param _nftContract Address of NFT contract
     */
    function addSupportedNftContract(address _nftContract) public onlyOwner {
        _allowedNftContracts[_nftContract] = true;
    }

    /**
     @notice Method for checking price asset is available
     @param _priceAsset Address of price asset contract
     */
    function isSupportedPriceAssets(address _priceAsset)
        public
        view
        returns (bool)
    {
        return _allowedPriceAssets[_priceAsset];
    }

    /**
     @notice Method for checking NFT contract is available
     @param _nftContract Address of NFT contract
     */
    function isSupportedNftContract(address _nftContract)
        public
        view
        returns (bool)
    {
        return _allowedNftContracts[_nftContract];
    }

    /**
     @notice Method for get unsold listings by NFT contract
     @param _nftContract Address of NFT contract
     */
    function getListedNfts(address _nftContract)
        public
        view
        returns (NFT[] memory)
    {
        uint256 nftCount = _nftCount.current();
        uint256 unsoldNftsCount = nftCount - _nftsSold.current();

        NFT[] memory nfts = new NFT[](unsoldNftsCount);
        uint256 nftsIndex = 0;
        uint256 listedLength = 0;
        for (uint256 i = 0; i < nftCount; i++) {
            if (_listOfNfts[_nftContract][i + 1].listed) {
                nfts[nftsIndex] = _listOfNfts[_nftContract][i + 1];
                nftsIndex++;
                listedLength++;
            }
        }

        NFT[] memory filteredNfts = new NFT[](listedLength);
        for (uint256 i = 0; i < nfts.length; i++) {
            if (nfts[i].listed) {
                filteredNfts[i] = nfts[i];
            }
        }
        return filteredNfts;
    }

    /**
     @notice Method for get user unsold listings by NFT contract
     @param _nftContract Address of NFT contract
     */
    function getMyListedNfts(address _nftContract)
        public
        view
        returns (NFT[] memory)
    {
        uint256 nftCount = _nftCount.current();
        uint256 myListedNftCount = 0;
        for (uint256 i = 0; i < nftCount; i++) {
            if (
                _listOfNfts[_nftContract][i + 1].seller == msg.sender &&
                _listOfNfts[_nftContract][i + 1].listed
            ) {
                myListedNftCount++;
            }
        }

        NFT[] memory nfts = new NFT[](myListedNftCount);
        uint256 nftsIndex = 0;
        for (uint256 i = 0; i < nftCount; i++) {
            if (
                _listOfNfts[_nftContract][i + 1].seller == msg.sender &&
                _listOfNfts[_nftContract][i + 1].listed
            ) {
                nfts[nftsIndex] = _listOfNfts[_nftContract][i + 1];
                nftsIndex++;
            }
        }
        return nfts;
    }
}
