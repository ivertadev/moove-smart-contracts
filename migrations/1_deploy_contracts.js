const MooveSneakers = artifacts.require("MooveSneakers");
const Marketplace = artifacts.require("Marketplace");
const MooveWatch = artifacts.require("MooveWatch");
const MooveToken = artifacts.require("MooveToken");
const MVTToken = artifacts.require("MVTToken");

module.exports = async function (deployer) {
  await deployer.deploy(MooveSneakers);
  await deployer.deploy(Marketplace);
  await deployer.deploy(MooveWatch);
  await deployer.deploy(MooveToken);
  await deployer.deploy(MVTToken);
};
